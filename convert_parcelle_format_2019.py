# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 15:37:04 2019

@author: lwenjuan

convert format parcelle data into standard one
"""

import pandas as pd
from shapely.geometry import Polygon

path_csv_in = 'R:/CLIENT_DATASET/INRA_toulouse/2019/1573044721.csv'
path_csv_out = 'R:/CLIENT_DATASET/INRA_toulouse/2019/parcelle_1573044721.csv'

data_in = pd.read_csv(path_csv_in,sep=';')
data_out = pd.DataFrame(columns=['plot_id','latitude','longitude'])


for ipoint, row in data_in.iterrows():
	region_polygon = data_in.loc[ipoint].ExperimentURI
	region_coordinate = region_polygon[10:-2].split(',')
	plot_id = data_in.loc[ipoint].ScientificObjectURI
	
	latitude_all = []
	longitude_all = []
	for i in range(0,size(region_coordinate)):
		if i==0:
			latitude_all.append(float(region_coordinate[i].split(' ')[0]))
			longitude_all.append(float(region_coordinate[i].split(' ')[1]))
		else:
			latitude_all.append(float(region_coordinate[i].split(' ')[1]))
			longitude_all.append(float(region_coordinate[i].split(' ')[2]))
	
	polygon_1 = Polygon((zip(latitude_all,longitude_all)))
	coords_new = list(polygon_1.convex_hull.exterior.coords)
	
	for j in range(0,4):
		temp_dict = {"plot_id":plot_id,
					   "longitude":coords_new[j][0],
					   "latitude":coords_new[j][1]}
		data_out = data_out.append(temp_dict,ignore_index=True)
		
data_out.to_csv(path_csv_out,index=False)